package service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	private Service s;

	@Before
	public void setUp() throws Exception {
		s = Service.getService();
	}

	@Test
	public void testSamletDosis() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 05), LocalDate.of(2015, 10, 20), p, l, 100);
		assertEquals(0, pn.samletDosis(), 0.01);

	}

	public void testSamletDosis2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 05), LocalDate.of(2015, 10, 20), p, l, 100);
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		assertEquals(100, pn.samletDosis(), 0.01);

	}

	public void testSamletDosis3() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 05), LocalDate.of(2015, 10, 20), p, l, 100);
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));

		int i = 0;
		while (i < 30) {
			s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 12));
			i++;
		}

		assertEquals(3000, pn.samletDosis(), 0.01);

	}

	// ------------------------------------------------------------------------

	@Test
	public void testDoegnDosis() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 20), LocalDate.of(2015, 10, 20), p, l, 100);

		assertEquals(0, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 20), LocalDate.of(2015, 10, 20), p, l, 100);
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));

		assertEquals(500, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis3() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 20), LocalDate.of(2015, 10, 22), p, l, 100);
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 21));

		assertEquals(250, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis4() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 20), LocalDate.of(2015, 10, 25), p, l, 100);
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 20));
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 24));

		assertEquals(100, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testGivDosis() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 05), LocalDate.of(2015, 10, 20), p, l, 100);

		assertEquals(0, pn.samletDosis(), 0.01);

	}

	@Test
	public void testGivDosis2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 05), LocalDate.of(2015, 10, 20), p, l, 100);
		s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 12));

		assertEquals(100, pn.samletDosis(), 0.01);

	}

	@Test
	public void testGivDosis3() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = s.opretPNOrdination(LocalDate.of(2015, 10, 05), LocalDate.of(2015, 10, 20), p, l, 100);
		int i = 0;
		while (i < 30) {
			s.ordinationPNAnvendt(pn, LocalDate.of(2015, 10, 12));
			i++;
		}

		assertEquals(3000, pn.samletDosis(), 0.01);

	}

}
