package service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {

	private Service s;

	@Before
	public void setUp() {
		s = Service.getService();
	}

	@Test
	public void testSamletDosis() {
		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		d.opretDosis(LocalTime.of(00, 00), 100);

		assertEquals(100, d.samletDosis(), 0.1);
	}

	@Test
	public void testSamletDosis1() {
		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		d.opretDosis(LocalTime.of(00, 00), 100);
		d.opretDosis(LocalTime.of(10, 00), 100);
		d.opretDosis(LocalTime.of(15, 00), 100);

		assertEquals(300, d.samletDosis(), 0.1);
	}

	@Test
	public void testSamletDosis2() {
		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		d.opretDosis(LocalTime.of(00, 00), 100);
		d.opretDosis(LocalTime.of(10, 00), 300);
		d.opretDosis(LocalTime.of(15, 00), 700);

		assertEquals(1100, d.samletDosis(), 0.1);
	}

	@Test
	public void testDoegnDosis() {

		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 02));
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		d.opretDosis(LocalTime.of(00, 00), 100);

		assertEquals(100, d.doegnDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis1() {

		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		d.opretDosis(LocalTime.of(00, 00), 100);

		assertEquals(50, d.doegnDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis2() {

		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		d.opretDosis(LocalTime.of(00, 00), 50);

		assertEquals(25, d.doegnDosis(), 0.1);

	}

	@Test
	public void testOpretDosis() {
		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		d.opretDosis(LocalTime.of(00, 00), 0);

		assertEquals(LocalTime.of(00, 00), d.getDoser().get(0).getTid());
		assertEquals(0, d.getDoser().get(0).getAntal(), 0.1);
	}

	@Test
	public void testOpretDosis1() {
		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		d.opretDosis(LocalTime.of(00, 00), 100);

		assertEquals(LocalTime.of(00, 00), d.getDoser().get(0).getTid());
		assertEquals(100, d.getDoser().get(0).getAntal(), 0.1);
	}

	@Test
	public void testOpretDosis2() {
		DagligSkaev d = new DagligSkaev(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03));
		d.opretDosis(LocalTime.of(10, 00), 200);

		assertEquals(LocalTime.of(10, 00), d.getDoser().get(0).getTid());
		assertEquals(200, d.getDoser().get(0).getAntal(), 0.1);
	}

}
