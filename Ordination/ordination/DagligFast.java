package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];
	private double morgenAntal;
	private double middagAntal;
	private double aftenAntal;
	private double natAntal;
	private double[] antal = { morgenAntal, middagAntal, aftenAntal, natAntal };

	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		super(startDen, slutDen);
		this.morgenAntal = morgenAntal;
		this.middagAntal = middagAntal;
		this.aftenAntal = aftenAntal;
		this.natAntal = natAntal;
		doser[0] = opretDosis(morgenAntal);
		doser[1] = opretDosis(middagAntal);
		doser[2] = opretDosis(aftenAntal);
		doser[3] = opretDosis(natAntal);
	}

	public Dosis opretDosis(double antal) {
		Dosis d = new Dosis(antal);
		return d;

	}

	public Dosis[] getDoser() {
		// return Arrays.copyOf(doser, doser.length);
		return doser;
	}

	@Override
	public double samletDosis() {
		int sum = 0;
		for (int i = 0; i < doser.length; i++) {
			sum += doser[i].getAntal() * this.antalDage();
		}
		return sum;
	}

	@Override
	public double doegnDosis() {
		double sum = this.samletDosis();
		int dage = this.antalDage();
		return sum / dage;
	}

	public double getMorgenAntal() {
		return morgenAntal;
	}

	public double getMiddagAntal() {
		return middagAntal;
	}

	public double getAftenAntal() {
		return aftenAntal;
	}

	public double getNatAntal() {
		return natAntal;
	}

	@Override
	public String getType() {
		return "DagligFast";
	}
}
