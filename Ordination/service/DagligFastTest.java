package service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligFastTest {

	private Service s;

	@Before
	public void setUp() {
		s = Service.getService();
	}

	@Test
	public void testSamletDosis() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 03), 0, 100, 0, 0);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(100, d.samletDosis(), 0.1);

	}

	@Test
	public void testSamletDosis2() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 7), 0, 100, 0, 0);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(500, d.samletDosis(), 0.1);

	}

	@Test
	public void testSamletDosis3() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 7), 0, 0, 0, 0);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(0, d.samletDosis(), 0.1);

	}

	@Test
	public void testSamletDosis4() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 7), 100, 200, 100, 100);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(2500, d.samletDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03), 50, 50, 50, 50);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		assertEquals(200, d.doegnDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis1() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 07), 100, 100, 100, 200);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		assertEquals(500, d.doegnDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis2() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03), 0, 0, 0, 0);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		assertEquals(0, d.doegnDosis(), 0.1);

	}

	@Test
	public void testDoegnDosis3() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 03), 500, 500, 500, 1000);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		assertEquals(2500, d.doegnDosis(), 0.1);

	}

	@Test
	public void testOpretDosis() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), 1, 1, 1, 1);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(0, d.opretDosis(0).getAntal(), 0.1);
	}

	@Test
	public void testOpretDosis1() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), 1, 1, 1, 1);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(1, d.opretDosis(1).getAntal(), 0.1);
	}

	@Test
	public void testOpretDosis2() {
		DagligFast d = new DagligFast(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), 1, 1, 1, 1);
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		assertEquals(2, d.opretDosis(2).getAntal(), 0.1);
	}

}
