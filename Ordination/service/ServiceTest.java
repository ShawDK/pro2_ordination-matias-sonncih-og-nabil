package service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ServiceTest {
	private Service s;

	@Before
	public void setUp() {
		s = Service.getService();
	}

	@Test
	public void testOpretPNOrdination() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
				s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretPNOrdination1() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		s.opretPNOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 03), p,
				s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1);
		assertEquals(1, p.getOrdinationer().size());
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretPNOrdination2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		try {
			p.getOrdinationer().clear();
			s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOpretDagligFastOrdination() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		DagligFast d = s.opretDagligFastOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
				s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 0, 0, 0, 0);
		assertEquals(0, d.getMorgenAntal(), 0.1);
		assertEquals(0, d.getMiddagAntal(), 0.1);
		assertEquals(0, d.getAftenAntal(), 0.1);
		assertEquals(0, d.getNatAntal(), 0.1);
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretDagligFastOrdination2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		DagligFast d = s.opretDagligFastOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
				s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1, 1, 1, 1);
		assertEquals(1, d.getMorgenAntal(), 0.1);
		assertEquals(1, d.getMiddagAntal(), 0.1);
		assertEquals(1, d.getAftenAntal(), 0.1);
		assertEquals(1, d.getNatAntal(), 0.1);
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretDagligFastOrdination3() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		try {
			p.getOrdinationer().clear();
			DagligFast d = s.opretDagligFastOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), -1, 0, 0, 0);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOpretDagligFastOrdination4() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		try {
			p.getOrdinationer().clear();
			DagligFast d = s.opretDagligFastOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 0, -1, 0, 0);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOpretDagligFastOrdination5() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		try {
			p.getOrdinationer().clear();
			DagligFast d = s.opretDagligFastOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 0, 0, -1, 0);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOpretDagligFastOrdination6() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		try {
			p.getOrdinationer().clear();
			DagligFast d = s.opretDagligFastOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 0, 0, 0, -1);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeSlet = { LocalTime.of(10, 00), LocalTime.of(11, 15), LocalTime.of(23, 30) };
		double[] antalEnheder = { 5.0, 8.0, 2.0 };

		p.getOrdinationer().clear();
		DagligSkaev d = s.opretDagligSkaevOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p, l,
				klokkeSlet, antalEnheder);
		assertEquals(d.getDoser().get(0).getAntal(), 5.0, 0.1);
		assertEquals(klokkeSlet[0], d.getDoser().get(0).getTid());
		assertEquals(d.getDoser().get(1).getAntal(), 8.0, 0.1);
		assertEquals(klokkeSlet[1], d.getDoser().get(1).getTid());
		assertEquals(d.getDoser().get(2).getAntal(), 2.0, 0.1);
		assertEquals(klokkeSlet[2], d.getDoser().get(2).getTid());
		assertEquals("Daglig skæv", p.getOrdinationer().get(0).getType());
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretDagligSkaevOrdination1() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeSlet = { LocalTime.of(10, 00), LocalTime.of(11, 15), LocalTime.of(23, 30) };
		double[] antalEnheder = { 5.0, 8.0, 2.0 };

		p.getOrdinationer().clear();
		DagligSkaev d = s.opretDagligSkaevOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 03), p, l,
				klokkeSlet, antalEnheder);
		assertEquals(d.getDoser().get(0).getAntal(), 5.0, 0.1);
		assertEquals(klokkeSlet[0], d.getDoser().get(0).getTid());
		assertEquals(d.getDoser().get(1).getAntal(), 8.0, 0.1);
		assertEquals(klokkeSlet[1], d.getDoser().get(1).getTid());
		assertEquals(d.getDoser().get(2).getAntal(), 2.0, 0.1);
		assertEquals(klokkeSlet[2], d.getDoser().get(2).getTid());
		assertEquals("Daglig skæv", p.getOrdinationer().get(0).getType());
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretDagligSkaevOrdination2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeSlet = { LocalTime.of(10, 00), LocalTime.of(11, 15) };
		double[] antalEnheder = { 5.0, 8.0 };

		p.getOrdinationer().clear();
		DagligSkaev d = s.opretDagligSkaevOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 03), p, l,
				klokkeSlet, antalEnheder);
		assertEquals(d.getDoser().get(0).getAntal(), 5.0, 0.1);
		assertEquals(klokkeSlet[0], d.getDoser().get(0).getTid());
		assertEquals(d.getDoser().get(1).getAntal(), 8.0, 0.1);
		assertEquals(klokkeSlet[1], d.getDoser().get(1).getTid());
		assertEquals("Daglig skæv", p.getOrdinationer().get(0).getType());
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretDagligSkaevOrdination3() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeSlet = { LocalTime.of(10, 00) };
		double[] antalEnheder = { 5.0 };

		p.getOrdinationer().clear();
		DagligSkaev d = s.opretDagligSkaevOrdination(LocalDate.of(2016, 03, 03), LocalDate.of(2016, 03, 03), p, l,
				klokkeSlet, antalEnheder);
		assertEquals(d.getDoser().get(0).getAntal(), 5.0, 0.1);
		assertEquals("Fucidin", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void testOpretDagligSkaevOrdination5() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Laegemiddel l = s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeSlet = { LocalTime.of(10, 00), LocalTime.of(11, 15), LocalTime.of(23, 30) };
		double[] antalEnheder = { 5.0, 8.0, 2.0 };

		p.getOrdinationer().clear();
		try {
			DagligSkaev d = s.opretDagligSkaevOrdination(LocalDate.of(2016, 03, 05), LocalDate.of(2016, 03, 02), p, l,
					klokkeSlet, antalEnheder);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOrdinationPNAnvendt() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		PN pnn = s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
				s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1);
		s.ordinationPNAnvendt(pnn, LocalDate.of(2016, 03, 04));

		assertEquals(LocalDate.of(2016, 03, 04), pnn.getDoser().get(0));
	}

	@Test
	public void testOrdinationPNAnvendt1() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		try {
			PN pnn = s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1);
			s.ordinationPNAnvendt(pnn, LocalDate.of(2016, 03, 07));
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testOrdinationPNAnvendt2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 63.4);

		p.getOrdinationer().clear();
		try {
			PN pnn = s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p,
					s.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"), 1);
			s.ordinationPNAnvendt(pnn, LocalDate.of(2016, 03, 01));
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "IllegalArgumentException forekommer");
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 24);
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");

		assertEquals(24, s.anbefaletDosisPrDoegn(p, l), 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn1() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 25);
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");

		assertEquals(37.5, s.anbefaletDosisPrDoegn(p, l), 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn2() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 26);
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");

		assertEquals(39, s.anbefaletDosisPrDoegn(p, l), 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn3() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 119);
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");

		assertEquals(178.5, s.anbefaletDosisPrDoegn(p, l), 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn4() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 120);
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");

		assertEquals(180, s.anbefaletDosisPrDoegn(p, l), 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn5() {
		Patient p = s.opretPatient("Jane Jensen", "121256-0512", 121);
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");

		assertEquals(242, s.anbefaletDosisPrDoegn(p, l), 0.01);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");
		Patient p1 = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Patient p2 = s.opretPatient("Finn Madsen", "070985-1153", 83.2);

		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p1, l, 1);
		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p2, l, 1);
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(10, 70, l));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel1() {
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");
		Patient p1 = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Patient p2 = s.opretPatient("Finn Madsen", "070985-1153", 83.2);

		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p1, l, 1);
		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p2, l, 1);
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(10, 50, l));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel2() {
		Laegemiddel l = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Styk");
		Patient p1 = s.opretPatient("Jane Jensen", "121256-0512", 63.4);
		Patient p2 = s.opretPatient("Finn Madsen", "070985-1153", 83.2);

		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p1, l, 1);
		s.opretPNOrdination(LocalDate.of(2016, 03, 02), LocalDate.of(2016, 03, 05), p2, l, 1);
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(10, 63.4, l));
	}

}
