package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	private ArrayList<LocalDate> doser = new ArrayList<>();

	private double antalEnheder;
	private int antalGangeGivet;

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
		this.antalGangeGivet = 0;
	}

	public ArrayList<LocalDate> getDoser() {
		return new ArrayList<>(doser);
	}

	public void addDoser(LocalDate ld) {
		doser.add(ld);
	}

	public void removeDoser(LocalDate ld) {
		doser.remove(ld);
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */

	public boolean givDosis(LocalDate givesDen) {
		boolean gyldig = false;
		if (givesDen.isBefore(getSlutDen()) && givesDen.isAfter(getStartDen()) || (givesDen.isEqual(getStartDen()))
				|| givesDen.isEqual(getSlutDen())) {
			gyldig = true;
			doser.add(givesDen);
			antalGangeGivet++;
		}

		return gyldig;
	}

	@Override
	public double doegnDosis() {
		int antalDage = 0;
		double samlet = 0.0;
		if (antalGangeGivet == 0) {
			samlet = 0.0;

		} else {
			antalDage = (int) (ChronoUnit.DAYS.between(doser.get(0), doser.get(doser.size() - 1)) + 1);
			samlet = this.samletDosis();
			return samlet / antalDage;
		}
		return samlet;
	}

	@Override
	public double samletDosis() {
		int antalDage = this.getAntalGangeGivet();
		double samlet = antalDage * this.antalEnheder;
		return samlet;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {

		return "PN";
	}

}
