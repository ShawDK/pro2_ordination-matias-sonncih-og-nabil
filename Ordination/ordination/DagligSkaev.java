package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}

	public void addDosis(Dosis d) {
		doser.add(d);
	}

	public void removeDosis(Dosis d) {
		doser.remove(d);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		doser.add(d);
	}

	@Override
	public double samletDosis() {
		double samlet = 0;
		for (Dosis d : doser) {
			samlet += d.getAntal();
		}

		return samlet;
	}

	@Override
	public double doegnDosis() {
		double samlet = this.samletDosis();
		int dage = this.antalDage();
		return samlet / dage;
	}

	@Override
	public String getType() {

		return "Daglig skæv";
	}
}
